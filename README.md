DEMO for Digicode
=================

Building locally
----------------

#### Prerequisites

* Latest [JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

#### Building binaries

`./mvnw package`

Running locally
---------------

Once built, the executable uber JAR file can be found at `target/geo-csv-maker-<version>-SNAPSHOT.jar`. In order to run it the following command can be used

`java -jar geo-csv-maker-<version>-SNAPSHOT.jar`

The program displays its usage info to the client with all options details. 
