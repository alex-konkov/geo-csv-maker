package com.alexkonkov.demo.service;

import com.alexkonkov.demo.pojo.GeoSuggestion;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;

/**
 * Service responsible for GEO suggestions.
 */
public class GeoSuggestService {

    private static final String GEO_SUGGEST_BASE_URL = "http://api.goeuro.com/api/v2/position/suggest/en/";
    private static final String MEDIA_TYPE_APPLICATION_JSON = "application/json";
    private static final String UTF_8 = "UTF-8";

    private ObjectMapper mapper = new ObjectMapper();

    private HttpClient httpClient;

    public GeoSuggestService(HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    /**
     * Gets suggestions using the given query string.
     *
     * @param q query string to search suggestions by
     * @return the list of suggestions
     * @throws IOException if there are some issues communicating to external services
     */
    public List<GeoSuggestion> suggest(String q) throws IOException {
        HttpGet rq = new HttpGet(GEO_SUGGEST_BASE_URL + URLEncoder.encode(q, UTF_8));
        rq.setHeader(HttpHeaders.ACCEPT, MEDIA_TYPE_APPLICATION_JSON);
        HttpResponse rs = httpClient.execute(rq);
        InputStream rsBodyInputStream = rs.getEntity().getContent();
        InputStream is = rsBodyInputStream;
        return Arrays.asList(mapper.readValue(is, GeoSuggestion[].class));
    }


    /**
     * Writes the given suggestions into the given output stream in CSV form.
     *
     * @param suggestions the suggestions list
     * @param withHeader if output has to contain CSV header line
     * @param out output stream for writing CSV lines to
     * @throws IOException if there are some problems when writing data to the output stream
     */
    public void writeCsv(List<GeoSuggestion> suggestions, boolean withHeader, OutputStream out) throws IOException {
        CsvMapper csvMapper = new CsvMapper();
        csvMapper.enable(JsonGenerator.Feature.IGNORE_UNKNOWN);
        CsvSchema schema = CsvSchema.builder()
            .addColumn("_id")
            .addColumn("name")
            .addColumn("type")
            .addColumn("latitude")
            .addColumn("longitude")
            .build();
        if (withHeader) {
            schema = schema.withHeader();
        }
        csvMapper.writer(schema).writeValue(out, suggestions);
    }

}
