package com.alexkonkov.demo.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * GEO suggestion item.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GeoSuggestion {

    @JsonProperty("_id")
    private Integer id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("type")
    private String type;

    @JsonProperty("geo_position")
    private GeoPosition geoPosition;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public GeoPosition getGeoPosition() {
        return geoPosition;
    }

    public void setGeoPosition(GeoPosition geoPosition) {
        this.geoPosition = geoPosition;
    }

    public BigDecimal getLatitude() {
        return geoPosition.getLatitude();
    }

    public BigDecimal getLongitude() {
        return geoPosition.getLongitude();
    }

}
