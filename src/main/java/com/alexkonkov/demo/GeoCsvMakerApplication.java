package com.alexkonkov.demo;

import com.alexkonkov.demo.pojo.GeoSuggestion;
import com.alexkonkov.demo.service.GeoSuggestService;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.converters.FileConverter;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * A main class of the console application's entry point.
 */
public class GeoCsvMakerApplication {

    public static void main(String[] argv) throws IOException {
        Args args = new Args();
        JCommander jCom = new JCommander(args, argv);

        // sometimes beauty requires hacks, so be it
        String jarName = new File(GeoCsvMakerApplication.class.getProtectionDomain()
            .getCodeSource().getLocation().getPath()).getName();

        jCom.setProgramName(String.format("java -jar %s", jarName));
        if (args.file == null || args.q == null) {
            jCom.usage();
            System.exit(1); // raising error
        }

        // HTTP client is managed outside
        CloseableHttpClient httpClient = HttpClients.createDefault();

        // the GEO suggestions service is instantiated
        GeoSuggestService geoSuggestService = new GeoSuggestService(httpClient);

        // getting suggestions
        List<GeoSuggestion> suggestions = geoSuggestService.suggest(args.q);

        // writing them to CSV file
        FileOutputStream fos = new FileOutputStream(args.file);

        // writing suggestions as CSV lines to the given output stream
        geoSuggestService.writeCsv(suggestions, args.header, fos);

        fos.close();

        // closing the employed HTTP client
        httpClient.close();
    }

    /* program arguments defined declaratively */
    static class Args {

        @Parameter(names = "-q", description = "Query string for GEO suggestions")
        String q;

        @Parameter(names = "-file", description = "The output CSV file", converter = FileConverter.class)
        File file;

        @Parameter(names = "-header", description = "CSV header line on/off option")
        boolean header = false;

    }

}
