package com.alexkonkov.demo.service;

import com.alexkonkov.demo.pojo.GeoSuggestion;
import org.apache.http.*;
import org.apache.http.conn.ConnectionRequest;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestExecutor;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class GeoSuggestServiceTests {

    @Test
    public void testSuggestionsForBerlinIntoCsv() throws InterruptedException, ExecutionException, IOException, HttpException {
        BasicHttpResponse httpRs = new BasicHttpResponse(HttpVersion.HTTP_1_1, 200, "OK");
        httpRs.setEntity(new InputStreamEntity(resolveResourceInputStream("Berlin.json")));

        // mock client that always returns the response above
        CloseableHttpClient httpClient = buildHttpClientThatAlwaysReturns(httpRs);

        // service instance
        GeoSuggestService geoSuggestService = new GeoSuggestService(httpClient);

        // suggestion objects
        List<GeoSuggestion> berlinSuggestions = geoSuggestService.suggest("Berlin");

        // output stream for CSV lines
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        // writing all suggestions in CSV form to the stream for further checks
        geoSuggestService.writeCsv(berlinSuggestions, false, baos);

        // building string out of the collected bytes
        String csvLines = new String(baos.toByteArray());

        // expected CSV lines for validation
        String expectedCsvLines =
            "376217,Berlin,location,52.52437,13.41053\n" +
            "333977,\"Berlin Ostbahnhof\",station,52.510972,13.434567\n";

        // validate all serialisation aspects at once
        Assert.assertEquals(expectedCsvLines, csvLines);
    }

    private InputStream resolveResourceInputStream(String resourceName) throws IOException {
        return getClass().getClassLoader().getResource(resourceName).openStream();
    }

    private CloseableHttpClient buildHttpClientThatAlwaysReturns(HttpResponse rs) throws IOException, HttpException, InterruptedException, ExecutionException {
        HttpRequestExecutor requestExecutor = Mockito.mock(HttpRequestExecutor.class);
        Mockito.when(
            requestExecutor.execute(Mockito.<HttpRequest>any(),
                Mockito.<HttpClientConnection>any(),
                Mockito.<HttpContext>any())).thenReturn(rs);
        HttpClientConnectionManager cm = Mockito.mock(HttpClientConnectionManager.class);
        HttpClientConnection conn = Mockito.mock(HttpClientConnection.class);
        ConnectionRequest connRequest = Mockito.mock(ConnectionRequest.class);
        Mockito.when(cm.requestConnection(
            Mockito.<HttpRoute>any(),
            Mockito.any())).thenReturn(connRequest);
        Mockito.when(connRequest.get(
            Mockito.anyLong(),
            Mockito.<TimeUnit>any())).thenReturn(conn);

        return HttpClients.custom()
            .setRequestExecutor(requestExecutor)
            .setConnectionManager(cm)
            .build();
    }

}
